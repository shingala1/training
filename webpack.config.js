var webpack = require('webpack');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
var config = {
    // TODO: Add common Configuration
    module: {},
};

var themeConfig = Object.assign({}, config, {
	entry:'./javascript/app.js',
	watch:true,
	output:{
		path:__dirname,
		filename:'theme/assets/bundle.js'
	},
	externals: {
		"jquery": "jQuery"
	},
	module: {
		rules: [
		{
			test: /\.scss$/,
			use: [
			{
				loader: MiniCssExtractPlugin.loader,
				options: {
              // you can specify a publicPath here
              // by default it use publicPath in webpackOptions.output
              publicPath: __dirname
          }
      },
      "css-loader",
      "sass-loader"
      ]
  }
  ]
},
plugins:[
new MiniCssExtractPlugin({
            // Options similar to the same options in webpackOptions.output
            // both options are optional
            filename: "theme/assets/bundle.scss.liquid"
        })
]
});
// Return Array of Configurations
module.exports = (env, argv) => {
	if (argv.mode === 'development') {
		// themeConfig.devtool = 'source-map';
	}
	return [
	themeConfig    
	];
};
